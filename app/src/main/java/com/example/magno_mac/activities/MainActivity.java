package com.example.magno_mac.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.magno_mac.ecualizacionimagenmc.R;
import com.example.magno_mac.models.Ecualizador;
import com.example.magno_mac.models.FiltroMediana;
import com.example.magno_mac.models.Foto;
import com.example.magno_mac.models.FotoAdapter;
import com.example.magno_mac.models.Histograma;

import java.io.FileInputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Histograma histograma;
    private ImageView imgvResultado;
    private ImageView imgOriginal;
    private Bitmap imagenOriginal;
    private Ecualizador ecualizador;
    private LinearLayout layout;
    private ArrayList<Foto> fotos;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        fotos = new ArrayList<>();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        layout = (LinearLayout) findViewById(R.id.progressbar_view);

        mRecyclerView = (RecyclerView) findViewById(R.id.lista_fotos);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new FotoAdapter(fotos,getApplicationContext());
        mRecyclerView.setAdapter(mAdapter);

        new Task().execute();


    }

    class Task extends AsyncTask<String, Integer, Boolean> {
        @Override
        protected void onPreExecute() {
            layout.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            layout.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);
        }

        @Override
        protected Boolean doInBackground(String... params) {
            histograma = new Histograma();
            String filename = getIntent().getStringExtra("image");

            try {
                FileInputStream is = openFileInput(filename);
                imagenOriginal = BitmapFactory.decodeStream(is);
                is.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                histograma.crearHistograma(imagenOriginal);
                int[] imagenOriginalHistograma = histograma.getHistograma();

                Bitmap imagenSinRuido = FiltroMediana.filtroMediana(imagenOriginal);
                histograma.crearHistograma(imagenSinRuido);
                int[] imagenSinRuidoHistograma = histograma.getHistograma();

                ecualizador = new Ecualizador(imagenSinRuido);
                Bitmap imagenEcualizada = ecualizador.equalizarImagen(histograma.getHistograma());
                histograma.crearHistograma(imagenEcualizada);
                int[] imagenEcualizadaHistograma = histograma.getHistograma();

                fotos.add(new Foto(imagenOriginal, imagenOriginalHistograma));
                fotos.add(new Foto(imagenSinRuido, imagenSinRuidoHistograma));
                fotos.add(new Foto(imagenEcualizada, imagenEcualizadaHistograma));

                Log.d("Resultado", "EXITO");
            } catch (Exception e) {
                Log.d("Resultado", "Ocurrió un error: " + e.getLocalizedMessage());
            }
            return null;
        }
    }
}