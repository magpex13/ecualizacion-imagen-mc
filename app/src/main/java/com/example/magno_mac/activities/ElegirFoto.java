package com.example.magno_mac.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.magno_mac.ecualizacionimagenmc.R;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class ElegirFoto extends AppCompatActivity {

    private Button buscar;
    private Bitmap imagenOriginal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elegir_foto);
        buscar = (Button)findViewById(R.id.button_empezar);
    }

    public void buscarImagen(View v) {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Uri targetUri = data.getData();
            try {
                imagenOriginal = BitmapFactory.decodeStream(getContentResolver().openInputStream(targetUri));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            try {
                //Write file
                String filename = "bitmap.png";
                FileOutputStream stream = this.openFileOutput(filename, Context.MODE_PRIVATE);
                imagenOriginal.compress(Bitmap.CompressFormat.PNG, 100, stream);

                //Cleanup
                stream.close();
                imagenOriginal.recycle();

                //Pop intent
                Intent procesar = new Intent(this, MainActivity.class);
                procesar.putExtra("image", filename);
                startActivity(procesar);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}