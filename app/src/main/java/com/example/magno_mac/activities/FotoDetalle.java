package com.example.magno_mac.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.magno_mac.ecualizacionimagenmc.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.XAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.io.FileInputStream;
import java.util.ArrayList;

public class FotoDetalle extends AppCompatActivity {

    Bitmap bitmap;
    ImageView imageView;
    LineChart chart;
    int[] histograma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foto_detalle);

        String filename = getIntent().getStringExtra("image");
        try {
            FileInputStream is = this.openFileInput(filename);
            bitmap = BitmapFactory.decodeStream(is);
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        histograma = getIntent().getIntArrayExtra("histograma");

        imageView = (ImageView)findViewById(R.id.foto_bitmap);
        chart = (LineChart)findViewById(R.id.histograma_chart);

        imageView.setImageBitmap(bitmap);

        llenarGrafico(chart);
    }

    private void llenarGrafico(LineChart chart) {
        ArrayList<Entry> entradas = new ArrayList<>();
        ArrayList<String> nombres = new ArrayList<>();

        for (int i = 0;i < histograma.length; i++) {
            entradas.add(new Entry(histograma[i], i));
            nombres.add("" + i);
        }

        LineDataSet set = new LineDataSet(entradas,"Frecuencia");
        set.setDrawFilled(true);
        set.setDrawCircles(false);
        set.setLabel("Frecuencia");


        LineData data = new LineData(nombres, set);

        chart.setData(data);
        chart.getLegend().setEnabled(false);
        chart.animateY(2000);
        chart.setFocusable(true);
        chart.setPinchZoom(true);
        chart.getAxisLeft().setEnabled(false);
        chart.setDescription("");
        chart.getAxisRight().setEnabled(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.setDrawGridBackground(false);

    }
}
