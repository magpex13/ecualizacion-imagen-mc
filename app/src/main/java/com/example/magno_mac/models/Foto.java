package com.example.magno_mac.models;

import android.graphics.Bitmap;

/**
 * Created by jamil on 11/14/15.
 */
public class Foto {
    Bitmap bitmap;
    int[] histograma;

    public Foto(Bitmap bitmap, int[] grafico) {
        this.bitmap = bitmap;
        this.histograma = grafico;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public int[] getHistograma() {
        return histograma;
    }

    public void setHistograma(int[] histograma) {
        this.histograma = histograma;
    }
}
