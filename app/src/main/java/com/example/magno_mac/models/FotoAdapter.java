package com.example.magno_mac.models;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.magno_mac.activities.FotoDetalle;
import com.example.magno_mac.activities.MainActivity;
import com.example.magno_mac.ecualizacionimagenmc.R;
import com.github.mikephil.charting.charts.LineChart;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * Created by jamil on 11/14/15.
 */
public class FotoAdapter extends RecyclerView.Adapter<FotoAdapter.ViewHolder> {

    ArrayList<Foto> fotos;
    Context context;

    public FotoAdapter(ArrayList<Foto> fotos, Context context) {
        this.fotos = fotos;
        this.context = context;
    }

    @Override
    public FotoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_foto_card, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(FotoAdapter.ViewHolder holder, int position) {

        holder.fotoBitmap.setImageBitmap(fotos.get(position).getBitmap());
        final Foto foto = fotos.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    //Write file
                    String filename = "bitmap2.png";
                    FileOutputStream stream = context.openFileOutput(filename, Context.MODE_PRIVATE);
                    foto.getBitmap().compress(Bitmap.CompressFormat.PNG, 100, stream);

                    //Cleanup
                    stream.close();

                    //Pop intent
                    Intent procesar = new Intent(context, FotoDetalle.class);
                    procesar.putExtra("histograma",foto.getHistograma());
                    procesar.putExtra("image", filename);
                    procesar.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    context.startActivity(procesar);

                    System.out.println("worx");
                } catch (Exception ex) {
                    System.out.println(ex.getLocalizedMessage());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return fotos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView fotoBitmap;
        CardView fotoCard;
        LineChart histograma;

        public ViewHolder(View itemView) {
            super(itemView);
            fotoCard = (CardView) itemView.findViewById(R.id.product_card);
            fotoBitmap = (ImageView) itemView.findViewById(R.id.foto_bitmap);
        }
    }
}